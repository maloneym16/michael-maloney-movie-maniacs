# Movie Mania
- Cierra Reese
- Mark Ipatzi
- Kevin Salmeron
- Michael Maloney
- Uzair Suleman

Experience movie madness with MOVIE MANIA!

Movie Mania is for those who love to watch movies and want to keep track of their favorites. You can search and browse movies, read their summaries, watch trailers, and see what services are allowing you to stream some of your favorite movies.

---

## Feature Functionality
- No matter if the user is logged in or logged out everyone will be able to see movies and their details
- Users will be able to sign up for an account
- Users will be able to log in and logout
- After logging in, users will be able to bookmark, view their bookmarks, delete bookmarks, and edit their account details
- Each user will be able to

---

## Design

- [API_Design](/docs/api-design.md)
- [Data_Model](/docs/datamodel.md)
- [GHI](/docs/ghi.md)
- [Integrations](/docs/integrations.md)

---

## Running the app

- Follow these steps to become a Movie Maniac and join our Movie Mania!
1. Clone the repository down to your local machine
2. CD into the repository folder locally
3. CD into the new project directory
4. Run docker volume create pg-admin
5. Run docker volume create postgres-data
6. Run docker compose build
7. Run docker compose up
